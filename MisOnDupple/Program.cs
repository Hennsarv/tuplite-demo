﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnDupple
{
    static class Program
    {
        public static (int Count,T Sum,double Avg) Agg<T>(this IEnumerable<T> mass) 
        {
            int count = 0;
            double avg = 0;
            
            List<T> n = new List<T> { }; 
            T sum = n.SingleOrDefault();
            dynamic sumd = sum;
            foreach(T x in mass)
            {
                count++;
                switch(x)
                {
                    case int i: sumd += i; break;
                    case decimal d: sumd += d; break;
                    case double d: sumd += d; break;
                    case float f: sumd += f; break;
                }
            }

            try
            {
                avg = sumd / (double)count;
            }
            catch { }

            return (count, n.SingleOrDefault() , avg);
        }

        static void Main(string[] args)
        {
            int[] arvud = { 1, 2, 3, 4, 5 };
            Console.WriteLine(arvud.Agg().Avg);
        }
    }
}
