﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnTupple
{
    static class Program
    {
        static void Main(string[] args)
        {
            int a = arvuta(3, 4).korrutis;

            int[] arvud = { 1, 2, 3, 4, 5, 6, 7 };

            int s;
            double b;
            (_, s, b) = arvud.Agr();

        }

        static (int summa,int korrutis) arvuta (int x, int y)
        {
            return (x + y, x * y);
        }

        static (int count, int sum, double avg) Agr(this IEnumerable<int> arvud)
        {
            int c = 0;
            int s = 0;

            foreach(int x in arvud)
            {
                c++; s += x;
            }

            return (c, s, (double)s / c);

        }


    }
}
